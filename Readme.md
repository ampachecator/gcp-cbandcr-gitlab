# Trigger build job

- [ ] [Post 1](https://medium.com/john-lewis-software-engineering/deploying-to-google-kubernetes-engine-from-gitlab-ci-feaf51dae0c1)
- [ ] [Post 2*](https://medium.com/google-cloud/deploy-to-cloud-run-using-gitlab-ci-e056685b8eeb) 
- [ ] [Test](https://about.gitlab.com/partners/technology-partners/google-cloud-platform/)

# Multi Steps Builds

## Sequential
````
stages:
  - compile
  - test
  - package
  - deploy
  
compile:
  stage: compile
  script: echo 'Compiling' > compiled.txt
  artifacts:
    paths: 
      - compiled.txt

test:
  stage: test
  script: echo 'All tested passed'

package: 
  stage: package
  script: cat compiled.txt | gzip > packaged.gzip
  artifacts:
    paths:
      - packaged.gzip
````

## Parallel
````
image: docker:latest

stages:
  - compile
  - test
  - package
  - deploy
  
compile:
  stage: compile
  script: echo 'Compiling' > compiled.txt
  artifacts:
    paths: 
      - compiled.txt

test:
  stage: test
  script: echo 'All tested passed'

package: 
  stage: package
  script: cat compiled.txt | gzip > packaged.gzip
  needs: ["compile", "test"]
  artifacts:
    paths:
      - packaged.gzip

deploy:
  stage: deploy
  image: google/cloud-sdk
  needs: ["package"]
````

# Notas de la Reunión.


**Nota:** * Demo basada en this post.
